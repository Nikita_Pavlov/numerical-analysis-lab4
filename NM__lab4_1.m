f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));

lp = -2; 
rp = 2;
N = 8;
step = (rp-lp)/(N-1); 

sc=2*pi/(rp-lp);

coefs = zeros(N*2+1);

coefs(1) = integral(@(x) f(sc*x),lp,rp)/(rp-lp);
for i=1:N
    coefs(i*2) = integral(@(x) f(sc*x).*cos(i*sc*x),lp,rp)/denom_f(sc*i,lp,rp);
    coefs(i*2+1) = integral(@(x) f(sc*x).*sin(i*sc*x),lp,rp)/denom_f(sc*i,lp,rp);
end

figure(1);
fplot(f,[lp rp]);
hold on;

fplot(@(x) four_pol(x,N,coefs),[lp rp]);

legend('original function', 'fourier');
grid
hold off

err = integral(@(x) (f(x)-four_pol(x,N,coefs)).^2,lp,rp)/integral(f,lp,rp);
disp(err);

function res = four_pol(x,N,coefs)
    res=coefs(1);
    for i=1:N
        res=res+coefs(2*i)*cos(i*x)+coefs(2*i+1)*sin(i*x);
    end
end

function res = denom_f(k,lp,rp)
    res = (2*(rp-lp)*k-sin(2*lp*k)+sin(2*rp*k))/(4*k);
end
