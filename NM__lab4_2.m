f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));
weight = @(x) 1./sqrt(1-x.^2);

global lp; lp =-2; 
global rp; rp = 2;
global N; N = 10;
step = (rp-lp)/(N-1); 

coefs = zeros(1,N);

for i=1:N
    coefs(i)=integral(@(x) weight(scale(x)).*f(scale(x)).*cos(i*acos(scale(x))),lp,rp)./denom_f(i,weight);
end

figure(1);
fplot(f,[lp rp]);
hold on;

fplot(@(x) real(cheb_pol(x,coefs)),[lp rp]);

legend('original function', 'chebyshev polynomial')
grid
hold off

err = integral(@(x) (f(x)-cheb_pol(x,coefs)).^2,lp,rp)/integral(f,lp,rp);
disp(err);

function res = cheb_pol(x,coefs)
    global N;
    res=0;
    for i=1:N
        res=res + coefs(i)*cos(i*acos(x));
    end
end

function res = denom_f(i,weight)
    global rp; global lp;
    %res = integral(@(x) weight(x).*(cos(i*acos(x))).^2,0,1);
    res = integral(@(x) weight(scale(x)).*(cos(i*acos(scale(x)))).^2,lp,rp);
end

function res = scale(x)
    global rp; global lp;
    res = (2*x - lp-rp)/(rp-lp);
end